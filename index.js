
/*
    При N = 1 очевидно, что побеждает B. При N = 2 победа за А, т.к. она уменьшает число до 1, а это зеркальная ситуация рассматриваемой вначале.
    Нечетное число после хода в любом случае превращается в четное, т.к. среди делителей нечетного числа нет четных чисел.
    Выигрышная стратегия при четном числе N - это каждый раз отнимать единицу, поскольку тогда число становится нечетным
    и каким бы изначально не было N рано или поздно число уменьшится до 1, а ход будет противника, ведь 1 - это нечетное число.
    Если число N - нечетное, Алиса проиграет при грамотной игре B, поскольку при любом ходе число станет четным.
*/
function findWinner(N) {
    return N % 2 === 0 ? 'A' : 'B';
}

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let winners = [];

rl.question('Enter amount of cases\n', (caseNumber) => {

    const readN = (i, cb) => {
        rl.question('Enter number N for case #' + i + '\n', (N) => {
            winners.push(findWinner(N));
            if (i == caseNumber) {
                cb()
            } else {
                readN(++i, cb);
            }
        });
    }

    readN(1, () => {
        rl.close();

        console.log('\nResult:');
        for (let i = 0; i < winners.length; i++) {
            console.log(winners[i]);
        }
    });

});